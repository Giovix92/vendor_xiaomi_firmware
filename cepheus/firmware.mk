# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/cepheus-firmware/cepheus/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi/cepheus-firmware/cepheus/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi/cepheus-firmware/cepheus/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi/cepheus-firmware/cepheus/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/imagefv.elf:install/firmware-update/imagefv.elf \
    vendor/xiaomi/cepheus-firmware/cepheus/km4.mbn:install/firmware-update/km4.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi/cepheus-firmware/cepheus/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi/cepheus-firmware/cepheus/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi/cepheus-firmware/cepheus/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
    vendor/xiaomi/cepheus-firmware/cepheus/xbl.elf:install/firmware-update/xbl.elf \
    vendor/xiaomi/cepheus-firmware/cepheus/xbl_config.elf:install/firmware-update/xbl_config.elf
